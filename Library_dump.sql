-- MySQL dump 10.13  Distrib 8.0.34, for Linux (x86_64)
--
-- Host: localhost    Database: Library
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Authors`
--

DROP TABLE IF EXISTS `Authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Authors` (
  `writer_id` int NOT NULL,
  `book_id` int NOT NULL,
  PRIMARY KEY (`writer_id`,`book_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `Authors_ibfk_1` FOREIGN KEY (`writer_id`) REFERENCES `Writers` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `Authors_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `Books` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Authors`
--

LOCK TABLES `Authors` WRITE;
/*!40000 ALTER TABLE `Authors` DISABLE KEYS */;
INSERT INTO `Authors` VALUES (3,1),(9,2),(4,3),(7,4),(1,5),(5,6),(6,7),(8,8),(2,9);
/*!40000 ALTER TABLE `Authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Books`
--

DROP TABLE IF EXISTS `Books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pages_amount` int NOT NULL,
  `publication_date` date DEFAULT NULL,
  `genre_id` int NOT NULL,
  `publisher_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_genre_id` (`genre_id`),
  KEY `idx_publisher_id` (`publisher_id`),
  KEY `idx_name_publication_date` (`name`,`publication_date`),
  CONSTRAINT `Books_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `Genres` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Books_ibfk_2` FOREIGN KEY (`publisher_id`) REFERENCES `Publishers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Books_chk_1` CHECK ((`pages_amount` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Books`
--

LOCK TABLES `Books` WRITE;
/*!40000 ALTER TABLE `Books` DISABLE KEYS */;
INSERT INTO `Books` VALUES (1,'Hundred Thousand',57,'2017-05-15',10,5),(2,'Kateryna',40,'2013-10-11',4,1),(3,'Marusya',320,'2001-01-13',11,4),(4,'Forest Song',137,'2022-09-26',4,6),(5,'Kaidash Family',243,'2011-05-07',2,5),(6,'I am (romance)',79,'2003-12-05',3,1),(7,'Instytutka',84,'2020-07-01',1,1),(8,'Eneyida',423,'2017-01-01',6,3),(9,'Zakhar Berkut',184,'2009-11-21',1,2);
/*!40000 ALTER TABLE `Books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Borrows`
--

DROP TABLE IF EXISTS `Borrows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Borrows` (
  `borrow_date` date NOT NULL,
  `return_date` date DEFAULT NULL,
  `book_id` int NOT NULL,
  `reader_id` int NOT NULL,
  `librarian_id` int NOT NULL,
  PRIMARY KEY (`book_id`,`reader_id`),
  KEY `reader_id` (`reader_id`),
  KEY `librarian_id` (`librarian_id`),
  CONSTRAINT `Borrows_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `Books` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Borrows_ibfk_2` FOREIGN KEY (`reader_id`) REFERENCES `Readers` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `Borrows_ibfk_3` FOREIGN KEY (`librarian_id`) REFERENCES `Librarians` (`person_id`) ON DELETE CASCADE,
  CONSTRAINT `Borrows_chk_1` CHECK ((`return_date` >= `borrow_date`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Borrows`
--

LOCK TABLES `Borrows` WRITE;
/*!40000 ALTER TABLE `Borrows` DISABLE KEYS */;
INSERT INTO `Borrows` VALUES ('2023-10-01',NULL,1,10,11),('2023-10-01',NULL,2,13,12),('2022-09-12',NULL,4,10,11),('2020-07-03','2020-10-17',6,10,11);
/*!40000 ALTER TABLE `Borrows` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `count_borrows_on_ins_trig` AFTER INSERT ON `Borrows` FOR EACH ROW UPDATE Library.Readers
     SET borrows_amount = borrows_amount + 1
   WHERE person_id = new.reader_id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `count_borrows_on_del_trig` BEFORE DELETE ON `Borrows` FOR EACH ROW UPDATE Library.Readers
     SET borrows_amount = borrows_amount - 1
   WHERE person_id = old.reader_id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Genres`
--

DROP TABLE IF EXISTS `Genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Genres` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Genres`
--

LOCK TABLES `Genres` WRITE;
/*!40000 ALTER TABLE `Genres` DISABLE KEYS */;
INSERT INTO `Genres` VALUES (1,'Novella'),(2,'Social novella'),(3,'Psych. novella'),(4,'Poem'),(5,'Historical dram. poem'),(6,'Burlesque poem'),(7,'Dramatic poem'),(8,'Poesy'),(9,'Play'),(10,'Comedy'),(11,'Dramatic novel');
/*!40000 ALTER TABLE `Genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Librarians`
--

DROP TABLE IF EXISTS `Librarians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Librarians` (
  `person_id` int NOT NULL,
  `employment_date` date DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  CONSTRAINT `Librarians_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `People` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Librarians`
--

LOCK TABLES `Librarians` WRITE;
/*!40000 ALTER TABLE `Librarians` DISABLE KEYS */;
INSERT INTO `Librarians` VALUES (11,'2010-01-01'),(12,'2023-10-01');
/*!40000 ALTER TABLE `Librarians` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `People`
--

DROP TABLE IF EXISTS `People`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `People` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `People`
--

LOCK TABLES `People` WRITE;
/*!40000 ALTER TABLE `People` DISABLE KEYS */;
INSERT INTO `People` VALUES (1,'Ivan','Nechuy-Levytsky'),(2,'Ivan','Franko'),(3,'Ivan','Karpenko-Kary'),(4,'Hryhorii','Kvitka'),(5,'Mykola','Fitilov'),(6,'Mariia','Vilinska'),(7,'Larysa','Kosach'),(8,'Ivan','Kotlyarevsky'),(9,'Taras','Shevchnko'),(10,'Nikita','Kliatskyi'),(11,'Anna','Petrochenko'),(12,'John','Doe'),(13,'Jane','Doe');
/*!40000 ALTER TABLE `People` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Publishers`
--

DROP TABLE IF EXISTS `Publishers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Publishers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Publishers`
--

LOCK TABLES `Publishers` WRITE;
/*!40000 ALTER TABLE `Publishers` DISABLE KEYS */;
INSERT INTO `Publishers` VALUES (1,'Independent'),(2,'Bohdan'),(3,'Bookchef'),(4,'Strelbytskyy'),(5,'Pulsars'),(6,'Folio');
/*!40000 ALTER TABLE `Publishers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Readers`
--

DROP TABLE IF EXISTS `Readers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Readers` (
  `person_id` int NOT NULL,
  `registration_date` date NOT NULL,
  `borrows_amount` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`person_id`),
  CONSTRAINT `Readers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `People` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Readers`
--

LOCK TABLES `Readers` WRITE;
/*!40000 ALTER TABLE `Readers` DISABLE KEYS */;
INSERT INTO `Readers` VALUES (1,'2020-09-12',0),(2,'2020-09-12',0),(3,'2020-09-12',0),(4,'2020-09-12',0),(5,'2020-09-12',0),(6,'2020-09-12',0),(7,'2020-09-12',0),(8,'2020-09-12',0),(9,'2020-09-12',0),(10,'2020-09-12',3),(13,'2023-10-01',1);
/*!40000 ALTER TABLE `Readers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Writers`
--

DROP TABLE IF EXISTS `Writers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Writers` (
  `person_id` int NOT NULL,
  `pen_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  CONSTRAINT `Writers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `People` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Writers`
--

LOCK TABLES `Writers` WRITE;
/*!40000 ALTER TABLE `Writers` DISABLE KEYS */;
INSERT INTO `Writers` VALUES (1,'Nechuy'),(2,'Myron'),(3,'Tobilevych'),(4,'Osnovianenko'),(5,'Khvylovy'),(6,'Vovchok'),(7,'Ukrainka'),(8,'Kotlyarevsky'),(9,'Kobzar');
/*!40000 ALTER TABLE `Writers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'Library'
--
/*!50003 DROP FUNCTION IF EXISTS `count_book_borrows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `count_book_borrows`(book_name VARCHAR(50)) RETURNS int
    READS SQL DATA
BEGIN
	DECLARE result INT;
	SELECT COUNT(*) INTO result FROM Library.Borrows WHERE book_id = (SELECT id from Library.Books WHERE name = book_name);
	RETURN result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddGenreWithResult` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddGenreWithResult`(
    IN genreName VARCHAR(40),
    OUT resultCode INT
)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET resultCode = -1;
    END;

    START TRANSACTION;

    SELECT COUNT(*) INTO @genreExists FROM Library.Genres WHERE name = genreName;
    
    IF @genreExists > 0 THEN
        ROLLBACK;
        SET resultCode = 1;
    ELSE
        INSERT INTO Library.Genres (name) VALUES (genreName);
        
        IF ROW_COUNT() > 0 THEN
            COMMIT;
            SET resultCode = 0;
        ELSE
            ROLLBACK;
            SET resultCode = -2;
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AddReader` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddReader`(
  IN p_name VARCHAR(50),
  IN p_surname VARCHAR(50),
  IN p_registration_date DATE,
  OUT p_reader_id INT
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    ROLLBACK;
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'An error occurred while adding the reader';
  END;

  START TRANSACTION;
  
  INSERT INTO People (name, surname)
  VALUES (p_name, p_surname);
  
  SET p_reader_id = LAST_INSERT_ID();
  
  INSERT INTO Readers (person_id, registration_date)
  VALUES (p_reader_id, p_registration_date);
  
  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DeleteGenreWithResult` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteGenreWithResult`(
    IN genreId INT,
    OUT resultCode INT
)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SET resultCode = -1;
    END;

    START TRANSACTION;

    SELECT COUNT(*) INTO @genreExists FROM Library.Genres WHERE id = genreId;
    
    IF @genreExists = 0 THEN
        ROLLBACK;
        SET resultCode = 1;
    ELSE
        DELETE FROM Library.Genres WHERE id = genreId;
        
        IF ROW_COUNT() > 0 THEN
            COMMIT;
            SET resultCode = 0;
        ELSE
            ROLLBACK;
            SET resultCode = -2;
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-04 12:28:25
