﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace web_dev_online_library.Models;

public partial class LibraryContext : DbContext
{
    public LibraryContext()
    {
    }

    public LibraryContext(DbContextOptions<LibraryContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Book> Books { get; set; }

    public virtual DbSet<Borrow> Borrows { get; set; }

    public virtual DbSet<Genre> Genres { get; set; }

    public virtual DbSet<Librarian> Librarians { get; set; }

    public virtual DbSet<Person> People { get; set; }

    public virtual DbSet<Publisher> Publishers { get; set; }

    public virtual DbSet<Reader> Readers { get; set; }

    public virtual DbSet<Writer> Writers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {}
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Book>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.HasIndex(e => e.GenreId, "idx_genre_id");

            entity.HasIndex(e => new { e.Name, e.PublicationDate }, "idx_name_publication_date");

            entity.HasIndex(e => e.PublisherId, "idx_publisher_id");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.GenreId).HasColumnName("genre_id");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");
            entity.Property(e => e.PagesAmount).HasColumnName("pages_amount");
            entity.Property(e => e.PublicationDate)
                .HasColumnType("date")
                .HasColumnName("publication_date");
            entity.Property(e => e.PublisherId).HasColumnName("publisher_id");

            entity.HasOne(d => d.Genre).WithMany(p => p.Books)
                .HasForeignKey(d => d.GenreId)
                .HasConstraintName("Books_ibfk_1");

            entity.HasOne(d => d.Publisher).WithMany(p => p.Books)
                .HasForeignKey(d => d.PublisherId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("Books_ibfk_2");
        });

        modelBuilder.Entity<Borrow>(entity =>
        {
            entity.HasKey(e => new { e.BookId, e.ReaderId }).HasName("PRIMARY");

            entity.HasIndex(e => e.LibrarianId, "librarian_id");

            entity.HasIndex(e => e.ReaderId, "reader_id");

            entity.Property(e => e.BookId).HasColumnName("book_id");
            entity.Property(e => e.ReaderId).HasColumnName("reader_id");
            entity.Property(e => e.BorrowDate)
                .HasColumnType("date")
                .HasColumnName("borrow_date");
            entity.Property(e => e.LibrarianId).HasColumnName("librarian_id");
            entity.Property(e => e.ReturnDate)
                .HasColumnType("date")
                .HasColumnName("return_date");

            entity.HasOne(d => d.Book).WithMany(p => p.Borrows)
                .HasForeignKey(d => d.BookId)
                .HasConstraintName("Borrows_ibfk_1");

            entity.HasOne(d => d.Librarian).WithMany(p => p.Borrows)
                .HasForeignKey(d => d.LibrarianId)
                .HasConstraintName("Borrows_ibfk_3");

            entity.HasOne(d => d.Reader).WithMany(p => p.Borrows)
                .HasForeignKey(d => d.ReaderId)
                .HasConstraintName("Borrows_ibfk_2");
        });

        modelBuilder.Entity<Genre>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasMaxLength(40)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Librarian>(entity =>
        {
            entity.HasKey(e => e.PersonId).HasName("PRIMARY");

            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.EmploymentDate)
                .HasColumnType("date")
                .HasColumnName("employment_date");

            entity.HasOne(d => d.Person).WithOne(p => p.Librarian)
                .HasForeignKey<Librarian>(d => d.PersonId)
                .HasConstraintName("Librarians_ibfk_1");
        });

        modelBuilder.Entity<Person>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasMaxLength(20)
                .HasColumnName("name");
            entity.Property(e => e.Surname)
                .HasMaxLength(40)
                .HasColumnName("surname");
        });

        modelBuilder.Entity<Publisher>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .HasMaxLength(40)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Reader>(entity =>
        {
            entity.HasKey(e => e.PersonId).HasName("PRIMARY");

            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.BorrowsAmount).HasColumnName("borrows_amount");
            entity.Property(e => e.RegistrationDate)
                .HasColumnType("date")
                .HasColumnName("registration_date");

            entity.HasOne(d => d.Person).WithOne(p => p.Reader)
                .HasForeignKey<Reader>(d => d.PersonId)
                .HasConstraintName("Readers_ibfk_1");
        });

        modelBuilder.Entity<Writer>(entity =>
        {
            entity.HasKey(e => e.PersonId).HasName("PRIMARY");

            entity.Property(e => e.PersonId).HasColumnName("person_id");
            entity.Property(e => e.PenName)
                .HasMaxLength(20)
                .HasColumnName("pen_name");

            entity.HasOne(d => d.Person).WithOne(p => p.Writer)
                .HasForeignKey<Writer>(d => d.PersonId)
                .HasConstraintName("Writers_ibfk_1");

            entity.HasMany(d => d.Books).WithMany(p => p.Writers)
                .UsingEntity<Dictionary<string, object>>(
                    "Author",
                    r => r.HasOne<Book>().WithMany()
                        .HasForeignKey("BookId")
                        .HasConstraintName("Authors_ibfk_2"),
                    l => l.HasOne<Writer>().WithMany()
                        .HasForeignKey("WriterId")
                        .HasConstraintName("Authors_ibfk_1"),
                    j =>
                    {
                        j.HasKey("WriterId", "BookId").HasName("PRIMARY");
                        j.ToTable("Authors");
                        j.HasIndex(new[] { "BookId" }, "book_id");
                        j.IndexerProperty<int>("WriterId").HasColumnName("writer_id");
                        j.IndexerProperty<int>("BookId").HasColumnName("book_id");
                    });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
