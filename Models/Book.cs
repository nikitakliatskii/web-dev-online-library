﻿using System;
using System.Collections.Generic;

namespace web_dev_online_library.Models;

public partial class Book
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int PagesAmount { get; set; }

    public DateTime? PublicationDate { get; set; }

    public int GenreId { get; set; }

    public int? PublisherId { get; set; }

    public virtual ICollection<Borrow> Borrows { get; set; } = new List<Borrow>();

    public virtual Genre Genre { get; set; } = null!;

    public virtual Publisher? Publisher { get; set; }

    public virtual ICollection<Writer> Writers { get; set; } = new List<Writer>();
}
