﻿using System;
using System.Collections.Generic;

namespace web_dev_online_library.Models;

public partial class Reader
{
    public int PersonId { get; set; }

    public DateTime RegistrationDate { get; set; }

    public int BorrowsAmount { get; set; }

    public virtual ICollection<Borrow> Borrows { get; set; } = new List<Borrow>();

    public virtual Person Person { get; set; } = null!;
}
