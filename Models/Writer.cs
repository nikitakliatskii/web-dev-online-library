﻿using System;
using System.Collections.Generic;

namespace web_dev_online_library.Models;

public partial class Writer
{
    public int PersonId { get; set; }

    public string? PenName { get; set; }

    public virtual Person Person { get; set; } = null!;

    public virtual ICollection<Book> Books { get; set; } = new List<Book>();
}
