﻿using System;
using System.Collections.Generic;

namespace web_dev_online_library.Models;

public partial class Borrow
{
    public DateTime BorrowDate { get; set; }

    public DateTime? ReturnDate { get; set; }

    public int BookId { get; set; }

    public int ReaderId { get; set; }

    public int LibrarianId { get; set; }

    public virtual Book Book { get; set; } = null!;

    public virtual Librarian Librarian { get; set; } = null!;

    public virtual Reader Reader { get; set; } = null!;
}
