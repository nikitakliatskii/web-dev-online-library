﻿using System;
using System.Collections.Generic;

namespace web_dev_online_library.Models;

public partial class Librarian
{
    public int PersonId { get; set; }

    public DateTime? EmploymentDate { get; set; }

    public virtual ICollection<Borrow> Borrows { get; set; } = new List<Borrow>();

    public virtual Person Person { get; set; } = null!;
}
