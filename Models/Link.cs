namespace web_dev_online_library.Models;

public record Link(string Name, string Controller, string Action, string Role = null);

