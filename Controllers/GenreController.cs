using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;
using MySql.Data;
using web_dev_online_library.Models;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Runtime.CompilerServices;

namespace web_dev_online_library.Controllers
{
    public class GenreController : Controller
    {
        private readonly LibraryContext _context;

        public GenreController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Genre
        public async Task<IActionResult> Index()
        {
	      var inStockQuery = FormattableStringFactory.Create("SELECT name FROM Library.Genres where id IN (SELECT genre_id FROM Library.Books GROUP BY genre_id)");
	      var inStock = _context.Database.SqlQuery<string>(inStockQuery);
	      ViewBag.InStock = inStock;
              return _context.Genres != null ? 
                          View(await _context.Genres.ToListAsync()) :
                          Problem("Entity set 'LibraryContext.Genres'  is null.");
        }

        // GET: Genre/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Genres == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // GET: Genre/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Genre/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Genre genre)
        {
            if (!ModelState.IsValid)
            {
		    return View(genre);
            }

	    MySqlParameter[] @params = 
	    {
		  new MySqlParameter("@genreName", MySqlDbType.VarChar, 40) {Direction = ParameterDirection.Input, Value = genre.Name},
		  new MySqlParameter("@returnVal", MySqlDbType.Int32) {Direction = ParameterDirection.Output}
	    };   

	    _context.Database.ExecuteSqlRaw("CALL AddGenreWithResult(@genreName, @returnVal)", @params);

	    int result = (int)@params[1].Value;
	    
	    return result switch
	    {
		0 => RedirectToAction(nameof(Index)),
		1 => Problem("Genre already exists"),
		-1 => Problem("Runtime error during genre creation"),
		-2 => Problem("Genre creation failed"),
		_ => Problem("Unknown genre creation error")

	    };
        }

        // GET: Genre/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Genres == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }
            return View(genre);
        }

        // POST: Genre/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Genre genre)
        {
            if (id != genre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(genre);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GenreExists(genre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(genre);
        }

        // GET: Genre/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Genres == null)
            {
                return NotFound();
            }

            var genre = await _context.Genres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // POST: Genre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Genres is null)
            {
                return Problem("Entity set 'LibraryContext.Genres'  is null.");
            }
	    MySqlParameter[] @params = 
	    {
		  new MySqlParameter("@genreId", MySqlDbType.Int32) {Direction = ParameterDirection.Input, Value = id},
		  new MySqlParameter("@returnVal", MySqlDbType.Int32) {Direction = ParameterDirection.Output}
	    };   

	    _context.Database.ExecuteSqlRaw("CALL DeleteGenreWithResult(@genreId, @returnVal)", @params);

	    int result = (int)@params[1].Value;
	    return result switch
	    {
		0 => RedirectToAction(nameof(Index)),
		1 => Problem("Genre not found"),
		-1 => Problem("Runtime error on genre deletion"),
		-2 => Problem("Genre deletion failed"),
		_ => Problem("Unknown genre deletion error")
	    };
        }

        private bool GenreExists(int id)
        {
          return (_context.Genres?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
