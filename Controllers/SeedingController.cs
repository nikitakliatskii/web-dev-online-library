using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using web_dev_online_library.Models;

namespace web_dev_online_library.Controllers
{
    public class SeedingController : Controller
    {
        private readonly LibraryContext _context;

        public SeedingController(LibraryContext context)
        {
            _context = context;
        }

        public IActionResult ExtendDbWithDummies()
        {
	    using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
		    var newPerson1 = new Person { Name = "John", Surname = "Doe"};
		    var newPerson2 = new Person { Name = "Jane", Surname = "Doe" };
		    var newLibrarian = new Librarian { Person = newPerson1, EmploymentDate = DateTime.Today };
		    var newReader = new Reader { Person = newPerson2, RegistrationDate = DateTime.Today };
		    var newBorrow1 = new Borrow { BorrowDate = DateTime.Now, ReturnDate = null, Book = _context.Books.FirstOrDefault(b => b.Name == "Kateryna"), Librarian = newLibrarian, Reader = newReader };
		    var newBorrow2 = new Borrow { BorrowDate = DateTime.Now, ReturnDate = null, Book = _context.Books.FirstOrDefault(b => b.Name == "Hundred Thousand"), Librarian = _context.Librarians.FirstOrDefault(l => l.Person.Name == "Anna"), Reader = _context.Readers.FirstOrDefault(r => r.Person.Name == "Nikita") };

		    _context.People.Add(newPerson1);
		    _context.People.Add(newPerson2);
		    _context.Librarians.Add(newLibrarian);
		    _context.Readers.Add(newReader);
		    _context.Borrows.Add(newBorrow1);
		    _context.Borrows.Add(newBorrow2);

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
            return RedirectToAction("Index", "Book");
        }

	public IActionResult FailExtendDbWithDummies()
	{
	    using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
		    var newPerson = new Person { Name = "J", Surname = "D"};
		    _context.People.Remove(_context.People.FirstOrDefault(p => p.Name == "J" && p.Surname == "D"));
		    var newBorrow = new Borrow { BorrowDate = DateTime.Now, ReturnDate = null, Book = _context.Books.FirstOrDefault(b => b.Name == "Hundred Thousand"), Librarian = _context.Librarians.FirstOrDefault(l => l.Person.Name == "J"), Reader = _context.Readers.FirstOrDefault(r => r.Person.Name == "J") };
		    var newLibrarian = new Librarian { Person = newPerson, EmploymentDate = DateTime.Today };
		    var newReader = new Reader { Person = newPerson, RegistrationDate = DateTime.Today };

		    _context.People.Add(newPerson);
		    _context.Librarians.Add(newLibrarian);
		    _context.Readers.Add(newReader);
		    _context.Borrows.Add(newBorrow);

                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
	    return RedirectToAction("Index", "Book");
	}
    }
}
