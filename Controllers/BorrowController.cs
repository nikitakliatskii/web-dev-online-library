using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using web_dev_online_library.Models;

namespace web_dev_online_library.Controllers
{
    public class BorrowController : Controller
    {
        private readonly LibraryContext _context;

        public BorrowController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Borrow
        public async Task<IActionResult> Index()
        {
            var libraryContext = _context.Borrows.Include(b => b.Book).Include(b => b.Librarian).Include(b => b.Reader);
            return View(await libraryContext.ToListAsync());
        }

        // GET: Borrow/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Borrows == null)
            {
                return NotFound();
            }

            var borrow = await _context.Borrows
                .Include(b => b.Book)
                .Include(b => b.Librarian)
                .Include(b => b.Reader)
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (borrow == null)
            {
                return NotFound();
            }

            return View(borrow);
        }

        // GET: Borrow/Create
        public IActionResult Create()
        {
            ViewData["BookId"] = new SelectList(_context.Books, "Id", "Id");
            ViewData["LibrarianId"] = new SelectList(_context.Librarians, "PersonId", "PersonId");
            ViewData["ReaderId"] = new SelectList(_context.Readers, "PersonId", "PersonId");
            return View();
        }

        // POST: Borrow/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BorrowDate,ReturnDate,BookId,ReaderId,LibrarianId")] Borrow borrow)
        {
            if (ModelState.IsValid)
            {
                _context.Add(borrow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Books, "Id", "Id", borrow.BookId);
            ViewData["LibrarianId"] = new SelectList(_context.Librarians, "PersonId", "PersonId", borrow.LibrarianId);
            ViewData["ReaderId"] = new SelectList(_context.Readers, "PersonId", "PersonId", borrow.ReaderId);
            return View(borrow);
        }

        // GET: Borrow/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Borrows == null)
            {
                return NotFound();
            }

            var borrow = await _context.Borrows.FindAsync(id);
            if (borrow == null)
            {
                return NotFound();
            }
            ViewData["BookId"] = new SelectList(_context.Books, "Id", "Id", borrow.BookId);
            ViewData["LibrarianId"] = new SelectList(_context.Librarians, "PersonId", "PersonId", borrow.LibrarianId);
            ViewData["ReaderId"] = new SelectList(_context.Readers, "PersonId", "PersonId", borrow.ReaderId);
            return View(borrow);
        }

        // POST: Borrow/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BorrowDate,ReturnDate,BookId,ReaderId,LibrarianId")] Borrow borrow)
        {
            if (id != borrow.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowExists(borrow.BookId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Books, "Id", "Id", borrow.BookId);
            ViewData["LibrarianId"] = new SelectList(_context.Librarians, "PersonId", "PersonId", borrow.LibrarianId);
            ViewData["ReaderId"] = new SelectList(_context.Readers, "PersonId", "PersonId", borrow.ReaderId);
            return View(borrow);
        }

        // GET: Borrow/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Borrows == null)
            {
                return NotFound();
            }

            var borrow = await _context.Borrows
                .Include(b => b.Book)
                .Include(b => b.Librarian)
                .Include(b => b.Reader)
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (borrow == null)
            {
                return NotFound();
            }

            return View(borrow);
        }

        // POST: Borrow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Borrows == null)
            {
                return Problem("Entity set 'LibraryContext.Borrows'  is null.");
            }
            var borrow = await _context.Borrows.FindAsync(id);
            if (borrow != null)
            {
                _context.Borrows.Remove(borrow);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BorrowExists(int id)
        {
          return (_context.Borrows?.Any(e => e.BookId == id)).GetValueOrDefault();
        }
    }
}
