using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using web_dev_online_library.Models;
using System.Data;
using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Runtime.CompilerServices;

namespace web_dev_online_library.Controllers
{
    public class BookController : Controller
    {
        private readonly LibraryContext _context;
	private readonly List<string> _fields = new List<string>
	{
		"None", "Name", "Publication Date", "Pages Amount"
	};

        public BookController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Book
        public async Task<IActionResult> Index(string? name, string? pagesAmount, string? genre, string? publisher, string? orderBy)
        {
	    var finalSelection = new List<Book>();
	    try
	    {
		    string filterQuery = "SELECT * FROM Library.Books";
		    var queryStrGenres = FormattableStringFactory.Create("SELECT name FROM Library.Genres");
		    var genres = await _context.Database?.SqlQuery<string>(queryStrGenres).ToListAsync();
		    genres.Insert(0, "All");
		    var genreSelection = new SelectList(genres, genre ?? "All");
		    ViewBag.GenreSelection = genreSelection;
		    var queryStrPublishers = FormattableStringFactory.Create("SELECT name FROM Library.Publishers");
		    var publishers = await _context.Database?.SqlQuery<string>(queryStrPublishers).ToListAsync();
		    publishers.Insert(0, "All");
		    var publisherSelection = new SelectList(publishers, publisher ?? "All");
		    ViewBag.PublisherSelection = publisherSelection;
		    var orderBySelection = new SelectList(_fields, orderBy);
		    ViewBag.OrderBySelection = orderBySelection;
		    List<string> valuesFilters = new List<string> ();
		    if (name is not null) 
		    {
			    ViewBag.FilterName = name ?? "";
			    valuesFilters.Add("name = \"" + name + '"');
		    } else ViewBag.FilterName = "";
		    if (pagesAmount is not null)
		    {
			ViewBag.FilterPagesAmount = pagesAmount;
			valuesFilters.Add("pages_amount " + pagesAmount);
		    } else ViewBag.FilterPagesAmount = "";
		    if (genre is not null && genre != "All")
		    {
			valuesFilters.Add("genre_id IN (SELECT id FROM Library.Genres WHERE Library.Genres.name = \"" + genre + "\")");
		    } 
		    if (publisher is not null && publisher != "All")
		    {
			valuesFilters.Add("publisher_id IN (SELECT id FROM Library.Publishers WHERE Library.Publishers.name = \"" + publisher + "\")");
		    }

		    if (valuesFilters.Count > 0)
		    {
			    filterQuery += " WHERE " + string.Join(" AND ", valuesFilters);
		    }

		    var queryStr = FormattableStringFactory.Create(filterQuery);
		    finalSelection = await _context.Books?.FromSql(queryStr)?.Include(b => b.Genre)?.Include(b => b.Publisher)?.ToListAsync() ;
		    if (orderBy is not null) {
			    switch (orderBy) {
				case "Name": 
					finalSelection.Sort((x, y) => x.Name.CompareTo(y.Name));
				break; case "Publication Date": 
					finalSelection.Sort((x, y) => DateTime.Compare(x.PublicationDate ?? new DateTime(), y.PublicationDate ?? new DateTime()));
				break; case "Pages Amount":
					finalSelection.Sort((x, y) => x.PagesAmount.CompareTo(y.PagesAmount));
				break; default: finalSelection.Sort((x, y) => x.Id.CompareTo(y.Id));
					break;
			    }
		    }
	    }
	    catch
	    {
		    finalSelection = await _context.Books?.Include(b => b.Genre)?.Include(b => b.Publisher)?.ToListAsync() ;
	    }
	    return View(finalSelection);
        }

        // GET: Book/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Books == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Genre)
                .Include(b => b.Publisher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

	    MySqlParameter[] @params = 
	    {
		  new MySqlParameter("@book_name", MySqlDbType.VarChar, 50) {Direction = ParameterDirection.Input, Value = book.Name},
		  new MySqlParameter("@book_borrows_amount", MySqlDbType.Int32) {Direction = ParameterDirection.Output}
	    };   

	    _context.Database.ExecuteSqlRaw("SELECT count_book_borrows(@book_name) into @book_borrows_amount", @params);

	    int borrowsAmount = (int)@params[1].Value;
	    ViewBag.BorrowsAmount = borrowsAmount;
            return View(book);
        }

        // GET: Book/Create
        public IActionResult Create()
        {
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Id");
            ViewData["PublisherId"] = new SelectList(_context.Publishers, "Id", "Id");
            return View();
        }

        // POST: Book/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,PagesAmount,PublicationDate,GenreId,PublisherId")] Book book)
        {
            if (ModelState.IsValid)
            {
                _context.Add(book);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Id", book.GenreId);
            ViewData["PublisherId"] = new SelectList(_context.Publishers, "Id", "Id", book.PublisherId);
            return View(book);
        }

        // GET: Book/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Books == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Id", book.GenreId);
            ViewData["PublisherId"] = new SelectList(_context.Publishers, "Id", "Id", book.PublisherId);
            return View(book);
        }

        // POST: Book/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,PagesAmount,PublicationDate,GenreId,PublisherId")] Book book)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GenreId"] = new SelectList(_context.Genres, "Id", "Id", book.GenreId);
            ViewData["PublisherId"] = new SelectList(_context.Publishers, "Id", "Id", book.PublisherId);
            return View(book);
        }

        // GET: Book/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Books == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Genre)
                .Include(b => b.Publisher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Books == null)
            {
                return Problem("Entity set 'LibraryContext.Books'  is null.");
            }
            var book = await _context.Books.FindAsync(id);
            if (book != null)
            {
                _context.Books.Remove(book);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int id)
        {
          return (_context.Books?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
