using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using web_dev_online_library.Models;

namespace web_dev_online_library.Controllers
{
    public class WriterController : Controller
    {
        private readonly LibraryContext _context;

        public WriterController(LibraryContext context)
        {
            _context = context;
        }

        // GET: Writer
        public async Task<IActionResult> Index()
        {
            var libraryContext = _context.Writers.Include(w => w.Person);
            return View(await libraryContext.ToListAsync());
        }

        // GET: Writer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Writers == null)
            {
                return NotFound();
            }

            var writer = await _context.Writers
                .Include(w => w.Person)
                .FirstOrDefaultAsync(m => m.PersonId == id);
            if (writer == null)
            {
                return NotFound();
            }

            return View(writer);
        }

        // GET: Writer/Create
        public IActionResult Create()
        {
            ViewData["PersonId"] = new SelectList(_context.People, "Id", "Id");
            return View();
        }

        // POST: Writer/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PersonId,PenName")] Writer writer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(writer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.People, "Id", "Id", writer.PersonId);
            return View(writer);
        }

        // GET: Writer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Writers == null)
            {
                return NotFound();
            }

            var writer = await _context.Writers.FindAsync(id);
            if (writer == null)
            {
                return NotFound();
            }
            ViewData["PersonId"] = new SelectList(_context.People, "Id", "Id", writer.PersonId);
            return View(writer);
        }

        // POST: Writer/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PersonId,PenName")] Writer writer)
        {
            if (id != writer.PersonId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(writer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WriterExists(writer.PersonId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.People, "Id", "Id", writer.PersonId);
            return View(writer);
        }

        // GET: Writer/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Writers == null)
            {
                return NotFound();
            }

            var writer = await _context.Writers
                .Include(w => w.Person)
                .FirstOrDefaultAsync(m => m.PersonId == id);
            if (writer == null)
            {
                return NotFound();
            }

            return View(writer);
        }

        // POST: Writer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Writers == null)
            {
                return Problem("Entity set 'LibraryContext.Writers'  is null.");
            }
            var writer = await _context.Writers.FindAsync(id);
            if (writer != null)
            {
                _context.Writers.Remove(writer);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WriterExists(int id)
        {
          return (_context.Writers?.Any(e => e.PersonId == id)).GetValueOrDefault();
        }
    }
}
